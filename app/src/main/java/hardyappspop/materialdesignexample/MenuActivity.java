package hardyappspop.materialdesignexample;

import android.animation.Animator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import hardyappspop.materialdesignexample.preference.MySharedPreference;

/**
 * Created by luisangelgarcia on 6/29/15.
 */
public class MenuActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private View viewContainer;

    private ImageButton imgCancel;
    private ImageButton imgAddImage;
    private TextView txtAddImage;

    private ImageButton imgAddImage01;
    private TextView txtAddImage01;
    private ImageButton imgAddImage02;
    private TextView txtAddImage02;
    private ImageButton imgAddImage03;
    private TextView txtAddImage03;

    private boolean isAnimated = false;

    private boolean isCancel = false;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isAnimated) {
            isAnimated = true;
            imgAddImage.setVisibility(View.VISIBLE);
            txtAddImage.setVisibility(View.VISIBLE);
            imgAddImage.animate().translationY(-150);
            txtAddImage.animate().translationY(-150);

            imgAddImage01.setVisibility(View.VISIBLE);
            txtAddImage01.setVisibility(View.VISIBLE);
            imgAddImage01.animate().translationY(-300);
            txtAddImage01.animate().translationY(-300);

            imgAddImage02.setVisibility(View.VISIBLE);
            txtAddImage02.setVisibility(View.VISIBLE);
            imgAddImage02.animate().translationY(-450);
            txtAddImage02.animate().translationY(-450);

            imgAddImage03.setVisibility(View.VISIBLE);
            txtAddImage03.setVisibility(View.VISIBLE);
            imgAddImage03.animate().translationY(-600);
            txtAddImage03.animate().translationY(-600);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        isCancel = false;
        if (v == imgCancel) {
            makeAnimation();
            isCancel = true;
        } else if (v == imgAddImage) {
            makeAnimation();
        } else if (v == imgAddImage01) {
            makeAnimation();
        } else if (v == imgAddImage02) {
            makeAnimation();
        } else if (v == imgAddImage03) {
            makeAnimation();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == viewContainer) {
            finish();
            return true;
        }
        return false;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        viewContainer = findViewById(R.id.view_container);
        imgCancel = (ImageButton) findViewById(R.id.img_cancel);
        imgAddImage = (ImageButton) findViewById(R.id.img_add_image);
        txtAddImage = (TextView) findViewById(R.id.txt_add_image);

        imgAddImage01 = (ImageButton) findViewById(R.id.img_add_image_01);
        txtAddImage01 = (TextView) findViewById(R.id.txt_add_image_01);
        imgAddImage02 = (ImageButton) findViewById(R.id.img_add_image_02);
        txtAddImage02 = (TextView) findViewById(R.id.txt_add_image_02);
        imgAddImage03 = (ImageButton) findViewById(R.id.img_add_image_03);
        txtAddImage03 = (TextView) findViewById(R.id.txt_add_image_03);

        viewContainer.setOnTouchListener(this);
        imgCancel.setOnClickListener(this);
        imgAddImage.setOnClickListener(this);
        imgAddImage01.setOnClickListener(this);
        imgAddImage02.setOnClickListener(this);
        imgAddImage03.setOnClickListener(this);
    }

    private void makeAnimation() {
        imgAddImage.animate().translationY(0);
        txtAddImage.animate().translationY(0);

        imgAddImage01.animate().translationY(0);
        txtAddImage01.animate().translationY(0);

        imgAddImage02.animate().translationY(0);
        txtAddImage02.animate().translationY(0);

        imgAddImage03.animate().translationY(0);
        txtAddImage03.animate().translationY(0);

        imgAddImage03.animate().setListener(animatorListener);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            txtAddImage.setVisibility(View.GONE);
            txtAddImage01.setVisibility(View.GONE);
            txtAddImage02.setVisibility(View.GONE);
            txtAddImage03.setVisibility(View.GONE);

            MySharedPreference mySharedPreference = new MySharedPreference(getApplicationContext());
            mySharedPreference.putBoolean(getApplicationContext().getResources().getString(R.string.sp_is_cancel),
                    isCancel);

            finish();
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };
}
