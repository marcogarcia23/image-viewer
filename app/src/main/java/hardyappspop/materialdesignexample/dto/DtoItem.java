package hardyappspop.materialdesignexample.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by luisangelgarcia on 6/29/15.
 */
public class DtoItem implements Parcelable {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    /**
     * Attribute to store the id that we could save in the database
     */
    private long id;
    /**
     * Attribute to store the url of the image that we will show
     */
    private String url;
    /**
     * Attribute to store the name that you will show for the image
     */
    private String name;
    // ===========================================================
    // Constructors
    // ===========================================================

    /**
     * Constructor by default
     */
    public DtoItem() {

    }

    /**
     * Constructor using the Parcel object
     *
     * @param in
     */
    public DtoItem(Parcel in) {
        this();
        id = in.readLong();
        url = in.readString();
        name = in.readString();
    }

    /**
     * Constructor using all the attributes
     *
     * @param id
     * @param url
     * @param name
     */
    public DtoItem(long id, String name, String url) {
        this();
        this.id = id;
        this.url = url;
        this.name = name;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(url);
        dest.writeString(name);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    public static final Creator<DtoItem> CREATOR = new Creator<DtoItem>() {
        @Override
        public DtoItem createFromParcel(Parcel source) {
            return new DtoItem(source);
        }

        @Override
        public DtoItem[] newArray(int size) {
            return new DtoItem[size];
        }
    };
}
