package hardyappspop.materialdesignexample.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import hardyappspop.materialdesignexample.R;
import hardyappspop.materialdesignexample.callback.CallBackItem;
import hardyappspop.materialdesignexample.dto.DtoItem;
import hardyappspop.materialdesignexample.holder.VhItem;

/**
 * Created by luisangelgarcia on 6/29/15.
 */
public class AdapterItem extends RecyclerView.Adapter<VhItem> {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String TAG = "AdapterItem";
    // ===========================================================
    // Fields
    // ===========================================================
    private List<DtoItem> items;
    private Context context;
    private CallBackItem callBackItem;
    // ===========================================================
    // Constructors
    // ===========================================================

    public AdapterItem(Context context, List<DtoItem> items, CallBackItem callBackItem) {
        this.context = context;
        this.items = items;
        this.callBackItem = callBackItem;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public VhItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.row_item, parent, false);
        return new VhItem(item, callBackItem);
    }

    @Override
    public void onBindViewHolder(VhItem holder, int position) {
        Picasso.with(context).load(items.get(position).getUrl()).into(holder.img);
        holder.txt.setText(items.get(position).getName());
        try {
            Bitmap bitmap = ((BitmapDrawable) holder.img.getDrawable()).getBitmap();
            if (bitmap != null) {
                generaColor(bitmap, holder.ll, holder.txt);
            }
        } catch (NullPointerException | ClassCastException e) {
            Log.e(TAG, "Error: " + e.getMessage());
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * Method to add a new item
     *
     * @param item
     */
    public void add(DtoItem item) {
        items.add(item);
        notifyDataSetChanged();
    }

    /**
     * Method to remove an item
     *
     * @param position
     */
    public void remove(int position) {
        items.remove(position);
        notifyDataSetChanged();
    }

    private void generaColor(Bitmap bitmap, final LinearLayout ll, final TextView txt) {
        // Asynchronous
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette p) {
                // Use generated instance

                Palette.Swatch vibrant = p.getVibrantSwatch();

                if (vibrant != null) {
                    ll.setBackgroundColor(vibrant.getRgb());
                    txt.setTextColor(vibrant.getTitleTextColor());
                }
            }
        });
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
