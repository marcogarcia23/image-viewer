package hardyappspop.materialdesignexample.fragment;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hardyappspop.materialdesignexample.MenuActivity;
import hardyappspop.materialdesignexample.R;
import hardyappspop.materialdesignexample.adapter.AdapterItem;
import hardyappspop.materialdesignexample.callback.CallBackItem;
import hardyappspop.materialdesignexample.dto.DtoItem;
import hardyappspop.materialdesignexample.preference.MySharedPreference;

/**
 * Created by luisangelgarcia on 6/29/15.
 */
public class FragmentImageViewer extends Fragment implements View.OnClickListener, CallBackItem {
    // ===========================================================
    // Constants
    // ===========================================================
    private final int COLS = 2;
    /**
     * The orientation for scrollview in the RecyclerView based on {@link StaggeredGridLayoutManager}
     * parameters.
     * <p/>
     * VERTICAL = 1
     * HORIZONTAL = 0
     */
    private final int ORITATION = 1;
    // ===========================================================
    // Fields
    // ===========================================================
    private View view;
    private ImageButton imgShowMenu;
    private boolean isShown = true;

    private RecyclerView rv;
    private StaggeredGridLayoutManager staggeredGLM;

    private AdapterItem adapter;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    private List<DtoItem> getItems() {
        List<DtoItem> items = new ArrayList<>();

        int cont = 0;

        items.add(new DtoItem(cont++, "Luffy",
                "http://i.kinja-img.com/gawker-media/image/upload/ffhxlgjixt7tckss6d3l.jpg"));
        items.add(new DtoItem(cont++, "Mina",
                "http://img1.wikia.nocookie.net/__cb20140119063210/onepiece/es/images/7/7b/Wake_up!.png"));
        items.add(new DtoItem(cont++, "Gol D. Roger",
                "http://www.onepiecelove.com/wp-content/uploads/2015/05/free-one-piece-wallpaper-526-hd-wallpaper.jpg"));
        items.add(new DtoItem(cont++, "Mina 2",
                "http://furahasekai.com/wp-content/uploads/2013/06/one-piece.jpg"));
        items.add(new DtoItem(cont++, "Ace",
                "http://www.onepiecelove.com/wp-content/uploads/2015/06/anime-one-piece-characters-hd-wallpaper-1920x1080.jpg"));
        items.add(new DtoItem(cont++, "Goku 1",
                "http://dragonblogz.com/wp-content/uploads/2014/10/Anime-One-Piece-Dragon-Ball-Z-Wallpaper.png"));
        items.add(new DtoItem(cont++, "Goku 2",
                "http://orig08.deviantart.net/0021/f/2013/312/c/6/one_piece_x_dragonball_z_by_naruke24-d6tgpi7.jpg"));
        items.add(new DtoItem(cont++, "One Piece",
                "http://4.bp.blogspot.com/-PnEhpSt25WY/UkZZs813RqI/AAAAAAAAAK8/3o1UhOA05Ec/s1600/op1.png"));

        items.add(new DtoItem(cont++, "Luffy",
                "http://i.kinja-img.com/gawker-media/image/upload/ffhxlgjixt7tckss6d3l.jpg"));
        items.add(new DtoItem(cont++, "Mina",
                "http://img1.wikia.nocookie.net/__cb20140119063210/onepiece/es/images/7/7b/Wake_up!.png"));
        items.add(new DtoItem(cont++, "Gol D. Roger",
                "http://www.onepiecelove.com/wp-content/uploads/2015/05/free-one-piece-wallpaper-526-hd-wallpaper.jpg"));
        items.add(new DtoItem(cont++, "Mina 2",
                "http://furahasekai.com/wp-content/uploads/2013/06/one-piece.jpg"));
        items.add(new DtoItem(cont++, "Ace",
                "http://www.onepiecelove.com/wp-content/uploads/2015/06/anime-one-piece-characters-hd-wallpaper-1920x1080.jpg"));
        items.add(new DtoItem(cont++, "Goku 1",
                "http://dragonblogz.com/wp-content/uploads/2014/10/Anime-One-Piece-Dragon-Ball-Z-Wallpaper.png"));
        items.add(new DtoItem(cont++, "Goku 2",
                "http://orig08.deviantart.net/0021/f/2013/312/c/6/one_piece_x_dragonball_z_by_naruke24-d6tgpi7.jpg"));
        items.add(new DtoItem(cont++, "One Piece",
                "http://4.bp.blogspot.com/-PnEhpSt25WY/UkZZs813RqI/AAAAAAAAAK8/3o1UhOA05Ec/s1600/op1.png"));

        items.add(new DtoItem(cont++, "Luffy",
                "http://i.kinja-img.com/gawker-media/image/upload/ffhxlgjixt7tckss6d3l.jpg"));
        items.add(new DtoItem(cont++, "Mina",
                "http://img1.wikia.nocookie.net/__cb20140119063210/onepiece/es/images/7/7b/Wake_up!.png"));
        items.add(new DtoItem(cont++, "Gol D. Roger",
                "http://www.onepiecelove.com/wp-content/uploads/2015/05/free-one-piece-wallpaper-526-hd-wallpaper.jpg"));
        items.add(new DtoItem(cont++, "Mina 2",
                "http://furahasekai.com/wp-content/uploads/2013/06/one-piece.jpg"));
        items.add(new DtoItem(cont++, "Ace",
                "http://www.onepiecelove.com/wp-content/uploads/2015/06/anime-one-piece-characters-hd-wallpaper-1920x1080.jpg"));
        items.add(new DtoItem(cont++, "Goku 1",
                "http://dragonblogz.com/wp-content/uploads/2014/10/Anime-One-Piece-Dragon-Ball-Z-Wallpaper.png"));
        items.add(new DtoItem(cont++, "Goku 2",
                "http://orig08.deviantart.net/0021/f/2013/312/c/6/one_piece_x_dragonball_z_by_naruke24-d6tgpi7.jpg"));
        items.add(new DtoItem(cont++, "One Piece",
                "http://4.bp.blogspot.com/-PnEhpSt25WY/UkZZs813RqI/AAAAAAAAAK8/3o1UhOA05Ec/s1600/op1.png"));

        items.add(new DtoItem(cont++, "Luffy",
                "http://i.kinja-img.com/gawker-media/image/upload/ffhxlgjixt7tckss6d3l.jpg"));
        items.add(new DtoItem(cont++, "Mina",
                "http://img1.wikia.nocookie.net/__cb20140119063210/onepiece/es/images/7/7b/Wake_up!.png"));
        items.add(new DtoItem(cont++, "Gol D. Roger",
                "http://www.onepiecelove.com/wp-content/uploads/2015/05/free-one-piece-wallpaper-526-hd-wallpaper.jpg"));
        items.add(new DtoItem(cont++, "Mina 2",
                "http://furahasekai.com/wp-content/uploads/2013/06/one-piece.jpg"));
        items.add(new DtoItem(cont++, "Ace",
                "http://www.onepiecelove.com/wp-content/uploads/2015/06/anime-one-piece-characters-hd-wallpaper-1920x1080.jpg"));
        items.add(new DtoItem(cont++, "Goku 1",
                "http://dragonblogz.com/wp-content/uploads/2014/10/Anime-One-Piece-Dragon-Ball-Z-Wallpaper.png"));
        items.add(new DtoItem(cont++, "Goku 2",
                "http://orig08.deviantart.net/0021/f/2013/312/c/6/one_piece_x_dragonball_z_by_naruke24-d6tgpi7.jpg"));
        items.add(new DtoItem(cont++, "One Piece",
                "http://4.bp.blogspot.com/-PnEhpSt25WY/UkZZs813RqI/AAAAAAAAAK8/3o1UhOA05Ec/s1600/op1.png"));

        return items;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_image_viewer, container, false);
        init();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isShown) {
            imgShowMenu.animate().translationY(0);
            isShown = true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        if (v == imgShowMenu) {
            if (isShown) {
                imgShowMenu.animate().translationY(300);
                isShown = false;
            }
        }
    }

    @Override
    public void onClickListener(int position) {
        Toast.makeText(getActivity(), "Pos: " + position, Toast.LENGTH_SHORT).show();
        adapter.remove(position);
    }

    @Override
    public void onLongClickListener(int position) {
        Toast.makeText(getActivity(), "Pos: " + position, Toast.LENGTH_SHORT).show();
        adapter.remove(position);
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        imgShowMenu = (ImageButton) view.findViewById(R.id.img_show_menu);
        imgShowMenu.setOnClickListener(this);
        imgShowMenu.animate().setListener(onAnimatorListener);

        rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        staggeredGLM = new StaggeredGridLayoutManager(COLS, ORITATION);
        rv.setLayoutManager(staggeredGLM);

        List<DtoItem> items = getItems();

        adapter = new AdapterItem(getActivity(), items, this);
        rv.setAdapter(adapter);
    }

    /**
     * Method to call an activity with no animation
     */
    private void startActivityNoAnimation() {
        Intent iMenu = new Intent(getActivity(), MenuActivity.class);
        //iMenu.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(iMenu);
    }

    /**
     * Method to call an activity with animation
     * Option 1
     */
    private void startActivityAnimation01() {
        Intent iMenu = new Intent(getActivity(), MenuActivity.class);
        iMenu.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(iMenu);
        getActivity().overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
    }

    /**
     * Method to call an activity with animation
     * Option 2
     */
    private void startActivityAnimation02() {
        Intent iMenu = new Intent(getActivity(), MenuActivity.class);
        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(), imgShowMenu, "");

        //ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeCustomAnimation(getActivity(),
        //      android.R.anim.fade_in, android.R.anim.fade_out);
        ActivityCompat.startActivity(getActivity(), iMenu, activityOptionsCompat.toBundle());
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private Animator.AnimatorListener onAnimatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (!isShown) {
                //startActivityNoAnimation();
                //startActivityAnimation01();
                startActivityAnimation02();
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };
}
