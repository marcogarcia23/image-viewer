package hardyappspop.materialdesignexample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import hardyappspop.materialdesignexample.fragment.FragmentImageViewer;
import hardyappspop.materialdesignexample.preference.MySharedPreference;

/**
 * Created by luisangelgarcia on 6/29/15.
 */
public class MainActivity extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String TAG_FRAGMENT_IMAGE_VIEWER = "FragmentImageViewer";
    // ===========================================================
    // Fields
    // ===========================================================
    private Toolbar toolbar;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        MySharedPreference mySharedPreference = new MySharedPreference(this);
        // is other button, show a popup to add an image
        if (!mySharedPreference.getBoolean(getResources().getString(R.string.sp_is_cancel), true)) {
            mySharedPreference.putBoolean(getResources().getString(R.string.sp_is_cancel), true);
            // show up a dialog
            Toast.makeText(this, "Is Other Button", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        addFragment(new FragmentImageViewer(), TAG_FRAGMENT_IMAGE_VIEWER);
    }

    /**
     * Method to add a new fragment to the view
     *
     * @param fragment
     * @param TAG
     */
    private void addFragment(Fragment fragment, final String TAG) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.frame_container, fragment, TAG).commit();
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
