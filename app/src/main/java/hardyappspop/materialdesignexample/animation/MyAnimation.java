package hardyappspop.materialdesignexample.animation;

import android.animation.Animator;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

/**
 * Created by luisangelgarcia on 6/29/15.
 */
public class MyAnimation {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public RotateAnimation getRotateAnimation() {
        RotateAnimation rotateAnimation = new RotateAnimation(0f, 135f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setRepeatCount(Animation.REVERSE);
        rotateAnimation.setStartOffset(0l);
        rotateAnimation.setDuration(200);

        return rotateAnimation;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public TranslateAnimation translateDown(float startX, float startY, float finalX, float finalY) {
        TranslateAnimation animation = new TranslateAnimation(startX, startY, finalX, finalY);
        animation.setDuration(5000);
        animation.setFillAfter(false);
        return animation;
    }

    /**
     * Method that return an {@link Animation} object with the animation that will perform the Item in the
     * recycler view when it is shown
     *
     * @param context
     * @return {@link Animation}
     */
    public Animation getAnimationItem(Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);

        return animation;
    }

    /**
     * Method that return an {@link Animator} object with the animation that will show the element
     * as a circularReveal.
     * This method just work for Lollipop and upper, if you're running an older version
     * it will return you null.
     *
     * @param view
     * @return {@link Animator}
     */
    public Animator showAsReveal(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            int cx = (view.getLeft() + view.getRight()) / 2;
            int cy = (view.getTop() + view.getBottom()) / 2;

            // get the final radius for the clipping circle
            int finalRadius = Math.max(view.getWidth(), view.getHeight());
            // create the animator for this view (the start radius is zero)
            Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy,
                    0, finalRadius);
            anim.setDuration(6000);

            view.setVisibility(View.VISIBLE);

            return anim;
        } else {
            return null;
        }
    }

    /**
     * Method that return an {@link Animator} object with the animation that will
     * hide the element as a CircularReveal.
     * This method just work for Lollipop and upper, if you're running an older version
     * it will return you null.
     *
     * @param view
     * @return {@link Animator}
     */
    public Animator hideAsReveal(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            int cx = (view.getLeft() + view.getRight()) / 2;
            int cy = (view.getTop() + view.getBottom()) / 2;

            // get the initial radius for the clipping circle
            int initialRadius = view.getWidth();

            // create the animation (the final radius is zero)
            Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy,
                    initialRadius, 0);
            anim.setDuration(1000);

            return anim;
        } else {
            return null;
        }
    }
    // ===========================================================
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
